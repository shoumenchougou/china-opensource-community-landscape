# openEuler 社区

## 社区简介

openEuler 是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目。

openEuler是面向数字基础设施的操作系统，支持服务器、 云计算、边缘计算、嵌入式等应用场景，支持多样性计算，致力于提供安全、稳定、易用的操作系统。通过为应用提供确定性保障能力，支持 OT 领域应用及 OT 与 ICT 的融合。

openEuler开源社区通过开放的社区形式与全球的开发者共同构建一个开放、多元和架构包容的软件生态体系，孵化支持多种处理器架构、覆盖数字设施全场景，推动企业数字基础设施软硬件、应用生态繁荣发展。

**openEuler主要包括两个代码仓库：**

代码仓: <https://gitee.com/openeuler>

软件包仓: <https://gitee.com/src-openeuler>

openEuler主要用于存放源码类项目。

src-openeuler主要用于存放制作发布件所需的软件包。

**如何加入**

加入方式参见：<https://www.openeuler.org/zh/community/contribution>

联系
网站：<https://openeuler.org>
邮箱：<contact@openeuler.io>