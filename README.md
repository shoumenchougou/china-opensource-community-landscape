# **中国开源社区 Landscape 社区** 

# **China Open Source Community Landscape Community （COSCLC）**


## **Landscape 社区介绍**

中国开源社区 Landscape 社区是以国内开源社区为单位的大型社区，社区成员包括国内的开源项目社区、综合性开源社区、官方背景开源社区等。

Landscape 社区所行之事：推动社区成员之间的活动、商务等形式的合作互助、为开源社区的项目及活动提供流量支持、组织社区成员分享开源项目的运营/商业化等经验、合力撰写中国开源社区的年度报告等工作。

让我们联合，共筑中国开源生态。Together，we will be stronger 。

## **全景图**

## 准入/退出机制

**准入条件**

进入 Landscape 社区的开源社区须符合以下前置条件（进入条件将动态调整）：

-   主仓库贡献者 ≥ 3
-   拥有 200 人以上的用户群组（微信/QQ等群组）
-   提供完整的社区介绍，包含社区简介、发展历史等内容，具体可参考根目录下的 “[PostgreSQL中⽂社区](https://gitee.com/h4cd/china-opensource-community-landscape/blob/master/PostgreSQL%E4%B8%AD%E6%96%87%E7%A4%BE%E5%8C%BA.md)”等社区介绍。

**退出条件**

-   成员主动退出
-   成员社区解散或项目停止维护

**_后续将根据每个开源社区的综合属性、活跃度对 Landscape 的成员进行分级，每个等级的权益不同。_**

## **Landscape 社区架构**

Landscape 社区采用社区治理模式，由委员会负责 Landscape 社区活动的统筹和规则制定；所有成员均可参与 Landscape 社区的各项活动，或发起新活动，亦可参与 Landscape 社区委员会的报名和选举。

<img width=400 src="./images/Landscape-structure.png">


### **关于 Landscape 委员会**

Landscape 委员会由成员投票选举选出，负责 Landscape 的工作推进和规定制定、实行。

**主要职责**

-   审核报名 Landscape 的开源社区资质
-   维护 Landscape 社区的行为准则，完善 Landscape 社区的各类规范
-   参与 LandScape 社区的活动制定/统筹
-   积极参与 Landscape 社区的各板块建设

**主要权益**

-   以 Landscape 社区为主体的活动流量倾斜
-   参与中国开源社区白皮书（年度报告）的策划与撰写
-   由 Landscape 社区定制的委员会证书、奖品（勋章/奖杯等）
-   OSCHINA 社区资源，包括但不限于推荐稿、年度大会资源等

**选举规则**

-   委员会规模：OSCHINA 常驻委员会 +４位动态委员会
-   选举方式：于 Landscape 交流群内报名 —— Landscape 成员投票 —— 统计产生投票结果
-   任期时间：每期三个月，到期后重选

**当期委员会**

-   **[OSCHINA 社区](https://www.oschina.net/)**
-   待定
-   待定
-   待定
-   待定

### 关于成员

Landscape 成员需积极参与各项活动，为社区的各项建设出谋划策。

## 当前活动

#### 1、短视频介绍

OSC 视频号《开源社区介绍》系列视频，以短视频方式对各开源社区进行曝光，并同步分发至各大短视频平台。

**本期介绍：**盛见开源社区

#### 2、COSCLC 社区畅聊系列直播

由 Landscape 社区提供运营资源（平台、宣传、海报等内容），合作成员提供内容和嘉宾，以直播的方式分享各大开源社区的项目或运营经验。

**当前直播：《COCL 社区畅聊之 Dromara ：聊聊 LiteFlow 与 Hertzbeat 》**，由 Dromara 社区的开源作者全面介绍 LiteFlow 和 Hertzbeat 项目。

#### **3、活动/商务合作推动**

成员社区寻求活动（直播/比赛/线下 Meeting 等）或商务合作时，可以由 Landscape 社区代为发布，对各社区发出邀请，提供更多的曝光/合作机会。

**当前活动：**待发布

## Landscape 社区行为准则

身为社区成员、贡献者和领导者，我们承诺使 Landscape 社区参与者不受骚扰，无论其年龄、体型、可见或不可见的缺陷、族裔、性征、性别认同和表达、经验水平、教育程度、社会与经济地位、国籍、相貌、种族、种姓、肤色、宗教信仰、或性取向如何。

我们致力于建立开放、友善、多样化、包容、健康的社区，并为此制定了 Landscape 社区行为准则 。

## **Landscape 交流群**

Landscape 社区交流群是社区成员的日常交流群，进群请添加微信：15875177872，或直接扫描下方二维码进行添加：


<img width=300 src="./images/osclqq.png">

原则上，Landscape 社区中的每个成员有社区代表进群即可，最佳模式是 1位社区核心成员+ 1 名社区运营同学。

## **Landscape 社区官网（待建设）**

Landscape 官网仍在建设中，目前已由 OSCHINA 开源中国社区提供**域名及服务器**支持，官网的前后端建设需要社区的力量，有意参与官网建设的社区/个人开发者均可报名，共建 Landscape 官网。

报名方式：

-   发起新 Issue ，提及自身擅长的技术栈，并留下邮箱等联系方式
-   进入 Landscape 微信群-添加群主（OSC罗奇奇）-注明来意

**感谢以下社区/个人为 Landscape 社区官网作出的贡献：**

-   资金支持：OSCHINA 社区（购买域名/服务器）
-   后台开发：
-   前端开发：

## 其他注意事项

### 完善社区信息

目前“[社区列表.md](https://gitee.com/h4cd/china-opensource-community-landscape/blob/master/%E7%A4%BE%E5%8C%BA%E5%88%97%E8%A1%A8.md)”页面为 Landscape 社区成员的汇总简介信息，仓库的根目录存放各开源社区的专属介绍文档（使用 Markdown 格式），可以描述得更加详尽，包括社区发展历史、规章、组织架构、团队建设、协作模型……（每个社区的治理模式都不一样，自由发挥描述即可）

### **关于仓库的图片插入**

-   请将 logo 图片存入 logo/ 目录，
-   其它图片存入 images/ 目录，
-   或者直接使用可用图片线上链接。