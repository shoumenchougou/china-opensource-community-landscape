# Apache RocketMQ 社区
## （一）社区简介
Apache RocketMQ 是由阿里巴巴开源的消息产品，历经多年双十一流量洪峰严苛考研，2016 年 RocketMQ 进入 Apache 孵化器并于 2017 年毕业，成为 Apache 基金会顶级开源项目。同一年，RocketMQ 商业版上线阿里云并对外提供商业云服务，开始了开源、商业、阿里集团三位一体协同并进、统一内核的演进道路。Apache RocketMQ 一直致力于打造金融级高可靠、高性能、低延迟的消息中间件，随着 RocketMQ 进入 Apache 基金会，RocketMQ 在金融级可用的极简架构、多副本能力、丰富的消息类型等方面有了长足的发展和进步。

## （二）发展现状
目前，全球 RocketMQ Contributors 接近 500 人，为了帮助社区开发者更好的找到感兴趣的方向，快速参与到社区，同时推动相关特性优化得以快速演进，RocketMQ 还成立了接近 30 多个 SIG（特别兴趣小组），其中包括内核、批、Connect、Streaming、多语言客户端、RocketMQ-Flink、Operator、Exporter 等不同兴趣小组。

## （三）治理模式
Apache RocketMQ 社区遵循 Apache 基金会规定的社区组织架构，由董事会（Board）决议设立项目管理委员会（PMC），负责社区的积极管理，促进整个社区长期和健康的发展。Apache RocketMQ 社区主要促进开发者关系、开发者培训、步道等工作。

## （四）运营实践
为更好聚集本地开发者，我们在北京、深圳、苏州等城市相继成立当地社区，并定期举行线下活动，共同讨论 RocketMQ 相关的落地实践与新的特性需求，大量创新从社区的各类活动中产生并且落地。从最近的几个版本来看，阿里巴巴以外的公司贡献的代码已经超过 60%，越来越多公司以及开发者参与进来，社区多样性得以充分发展。

除此之外，RocketMQ 还非常重视社区间的合作，先后与 Apache Flink，Apache Doris，Apache EventMesh，Apache APISIX，Apache Hudi，Apache Kyuubi等社区组织了多次联合Meetup，在打造 RocketMQ 上下游生态的同时，也为不同社区开发者近距离讨论提供了平台。

目前，全球超过数万家企业在使用 Apache RocketMQ，这其中不仅有字节、快手、小米、滴滴、同城艺龙等互联网头部企业，还有头部银行、券商、保险，基金公司等要求严苛的金融公司。经过多年发展，RocketMQ 已成为微服务领域业务消息首选。

伴随着云原生时代的到来以及实时计算的兴起， 生于云、长于云的 RocketMQ 5.0 应运而生，全新升级为云原生消息、事件、流融合处理平台，帮助用户更容易地构建下一代事件驱动和流处理应用。

Apache RocketMQ 先后获得第十六届中日韩东北亚开源软件优胜奖、OSCHINA 评选的最佳开源软件、中国开源云联盟优秀开源项目等奖项。
![输入图片说明](images/image6.jpeg)
![输入图片说明](images/image7.jpeg)